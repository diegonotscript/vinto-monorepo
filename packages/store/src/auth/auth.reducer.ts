import { createSlice } from '@reduxjs/toolkit'

const initialState = { 
    loading: false,
    error: false,
    auth: []
}

export const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {},
  extraReducers: { },
})

export default authSlice.reducer