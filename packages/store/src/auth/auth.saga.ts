import {
    takeLatest,
    call,
    put
  } from 'redux-saga/effects'
import { AuthTypes,IAuthLogin,AuthUser } from './auth.types'
import api,{AxiosResponse}  from '@vinto/services'
import { actions} from '../auth/auth.actions'
function* doLogin({payload}:IAuthLogin ) {
    try {
      const response: AxiosResponse<AuthUser> = yield call(api.post,JSON.stringify(payload))
      yield put(actions.authenticationSuccess(response.data))
    } catch (e) {
      yield put(actions.authenticationFailure({error: "Ocorreu um erro"}))
    }
  }
  

  
export default [
    takeLatest(AuthTypes.AUTH_LOGIN, doLogin)
  ]
  