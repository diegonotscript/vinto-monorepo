import { createAction } from '@reduxjs/toolkit'

import {
    AuthTypes,
    AuthLogin,
    AuthUser
} from './auth.types'

const authenticationLoading = createAction<{loading: boolean}>(AuthTypes.AUTH_LOADING)
const authentication = createAction<AuthLogin>(AuthTypes.AUTH_LOGIN)
const authenticationSuccess = createAction<AuthUser>(AuthTypes.AUTH_SUCCESS)
const authenticationFailure = createAction<{error: string}>(AuthTypes.AUTH_ERROR)
const logout = createAction<void>(AuthTypes.AUTH_LOGOUT)
const actions = {
    authentication,
    logout,
    authenticationSuccess,
    authenticationFailure,
    authenticationLoading
}

export { actions }