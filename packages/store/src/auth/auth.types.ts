import { PayloadAction } from '@reduxjs/toolkit'

/**
 * Action types
 */
export enum AuthTypes {
  AUTH_LOGIN = '@@AUTH/ATTEMPT',
  AUTH_LOGOUT = '@@AUTH/LOGOUT',
  AUTH_SUCCESS ='@@AUTH/SUCCCESS',
  AUTH_ERROR = '@@AUTH/ERROR',
  AUTH_LOADING = '@@AUTH/LOADING'
}

// /**
//  * State type
//  */

// export interface EmpresaState { }

/**
 * Interfaces
 */
export interface IUser {
    user_login: string;
    name: string;
    email: string;
    created_at: Date
    status: 0,
    roles: Array<string>
    capabilities?: {
      api: boolean,
      api_post: boolean,
      read: boolean,
      customer: boolean
    }
}
export type AuthUser = {
  status: string;
  message: string;
  data: IUser[];
  token: string;
}


export interface AuthLogin {
  username: string,
  password: string
}
export type IAuthLogin = PayloadAction<AuthLogin>;
export type IAuthLogout = PayloadAction<any>;
