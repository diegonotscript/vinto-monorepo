import { all } from 'redux-saga/effects'
import authSaga from './auth/auth.saga'

export default function* root() {
  yield all([...authSaga])
}
