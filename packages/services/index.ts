import axios,{AxiosResponse} from 'axios'


export default axios.create({
    "baseURL": "https://dev.vintowiki.com/vinto-api/v1/",
    headers: {
        "Content-Type" : "application/json"
    }
})

export {
    AxiosResponse
}